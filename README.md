# Elasticsearch + Flask
A simple Flask app that uses the [pyelasticsearch](https://pyelasticsearch.readthedocs.io/en/latest/) client to allow you to search a locally hosted Elasticsearch index.

## How to use it

Clone the repo. Create a virtual environment and install the requirements: `pip install -r requirements.txt`.  

[Download Elasticsearch](https://www.elastic.co/products/elasticsearch) and start it up on your machine. Make sure it's running (`curl localhost:9200` should return some JSON that ends with "you know, for search").

Create an index and upload some data into it. 

Now you need to make a few changes to a few files in your local ElasticSnoop repo:  
In `elasticsnoop.py`, replace `index_name` (in the `get_search_query()` function) with the name of the index you created.

In `templates/search_results.html`, tweak this section:
```
{% for doc in results['hits']['hits'] %}
<p>{{ doc['_source']['Title'] }}</p>
{% endfor %}
``` 
...replacing doc['_source']['Title'] with the name of the field you want to see in the results (e.g. `doc['_source']['category']` if the desired field is 'category').

Also feel free to modify `get_search_query()` to support more complex queries. Consult the [pyelasticsearch docs](https://pyelasticsearch.readthedocs.io/en/latest/) for more info on that.

## What does it look like?
Here's the homepage:
![Screen Shot 2016-10-04 at 1.01.40 PM.png](https://bitbucket.org/repo/gjzdA8/images/3540578136-Screen%20Shot%202016-10-04%20at%201.01.40%20PM.png)

And the search results page:
![Screen Shot 2016-10-04 at 1.01.24 PM.png](https://bitbucket.org/repo/gjzdA8/images/4117216737-Screen%20Shot%202016-10-04%20at%201.01.24%20PM.png)

## Some background info
You may wonder why this app even exists. It doesn't have a lot of the functionality that Elasticsearch's Sense app does, so why bother? Basically, I made it because I love Python and I wanted to play around with Flask. I'm sharing it here because I didn't see many Python/Flask + Elasticsearch projects out there, and I thought it might help or inspire a kindred spirit who wants to experiment with these two technologies.