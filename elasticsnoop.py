from flask import Flask, render_template, redirect, request, url_for
from pyelasticsearch import ElasticSearch
import re

app = Flask(__name__)

# replace "localhost" as needed
es = ElasticSearch("http://localhost:9200")
results = {}
search_query = ""

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/search', methods=['POST'])
def search():
    num_results = 0
    searched = False
    if request.method == 'POST':
        search_term = request.form['search']
        if search_term:
            results = get_search_query(search_term)
            # see https://pyelasticsearch.readthedocs.io/en/latest/ for structure of search result
            num_results = results['hits']['total']
            # indicate that the search was successful
            searched = True
        else:
            results = "You didn't enter a search! Try again."
    return render_template('search_results.html', results=results, num_results=num_results, search_status=searched)


def get_search_query(search_request):
    # replace "dd-blog" with the index you want to search
    index_name="dd-blog"
    search_term = search_request
    result = es.search(search_term, index="dd-blog")
    return result

@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404

@app.errorhandler(500)
def search_failed(error):
    return render_template('404.html'), 500

if __name__ == '__main__':
    app.run(debug=True)
